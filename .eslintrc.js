module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true,
        "es6": true,
        "jquery": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parser": "babel-eslint",
    "parserOptions": {
        "sourceType": "module",
        "allowImportExportEverywhere": true,
        "ecmaVersion": 12

    },
    "globals": {
        'Swiper': true
    },
    "rules": {
    }
};
