# gulp-scss-starter

## Features
* class naming by [БЭМ](https://ru.bem.info/)
* BEM structure is used
* preprocessor is used [SCSS](https://sass-lang.com/)
* transpiler used [Babel](https://babeljs.io/) to support modern JavaScript (ES6) in browsers
* is used [Webpack](https://webpack.js.org/) to build JavaScript modules
* using a strict code guide
* check the code for errors before committing

## Setup
* install the [NodeJS](https://nodejs.org/en/) (If you want) и [Yarn](https://yarnpkg.com/en/docs/install)
* install the  ```gulp``` global: ```npm i -g gulp-cli```
* install the ```bem-tools-core``` global: ```npm i -g bem-tools-core```
* setup the eslint ```./node_modules/.bin/eslint --init```
* download the required dependencies: ```npm i```
* to get started, enter the command: ```npm run dev``` (dev mode)
* to build the project, enter the command ```npm run build``` (build mode)

If you did everything correctly, you should open a browser with a local server. Build mode assumes project optimization: image compression, minification of CSS and JS files for uploading to the server.

## File structure

```
gulp-scss-starter
├── dist
├── gulp-tasks
├── src
│   ├── blocks
│   ├── fonts
│   ├── img
│   ├── js
│   ├── styles
│   ├── views
│   └── .htaccess
├── gulpfile.babel.js
├── webpack.config.js
├── package.json
├── .babelrc.js
├── .bemrc.js
├── .eslintrc.json
├── .stylelintrc
├── .stylelintignore
└── .gitignore
```

* Folder root:
    * ```.babelrc.js``` — customization Babel
    * ```.bemrc.js``` — customization БЭМ
    * ```.eslintrc.json``` — customization ESLint
    * ```.gitignore``` – excluding files from Git
    * ```.stylelintrc``` — customization Stylelint
    * ```.stylelintignore``` – excluding files from Stylelint
    * ```gulpfile.babel.js``` — customization Gulp
    * ```webpack.config.js``` — customization Webpack
    * ```package.json``` — list of dependencies
    
* Folder ```src``` - used during development:
    * BEM blocks: ```src/blocks```
    * fonts: ```src/fonts```
    * images: ```src/img```
    * JS-files: ```src/js```
    * pages: ```src/views/pages```
    * SCSS-files: ```src/styles```
    * HTML-files: ```src/views```
    * Apache web server configuration file with settings [gzip](https://habr.com/ru/post/221849/) (lossless compression): ```src/.htaccess```

* Folder ```dist``` - the folder from which the local development server starts (at startup ```npm run dev```)

* Folder ```gulp-tasks``` - folder with Gulp-tasks

## Commands
* ```yarn run lint:styles``` check SCSS files. For VSCode you need to install -  [плагин](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint). For WebStorm or PHPStorm must enable Stylelint in ``` Languages & Frameworks - Style Sheets - Stylelint ``` (errors will be fixed automatically when you save the file).
* ```npm run lint:styles --fix``` - fix errors in SCSS files
* ```npm run lint:scripts``` - check JS files
* ```npm run lint:scripts --fix``` - fix errors in JS files
* ```npm run dev``` - server launch for project development
* ```npm run build``` - build a project with optimization without starting the server
* ```npm run build:views``` - collect HTML files
* ```npm run build:styles``` - compile SCSS files
* ```npm run build:scripts``` - collect JS files
* ```npm run build:images``` - collect images
* ```npm run build:sprites```- collect sprites
* ```npm run build:fonts``` - collect fonts
* ```npm run build:favicons``` - collect favicons
* ```npm run build:gzip``` - collect Apache config
* ```npm run bem-m``` - add BEM block
* ```npm run bem-c``` - add component

## Recommendations for use
### Component approach to website development
* each BEM block has its own folder inside ```src/blocks/modules```
* folder of one BEM block contains one HTML file, one SCSS file and one JS file (if the block uses a script)
    * Block HTML file is imported to file ```src/views/index.html``` (or to the required page file where the block will be called from)
    * The SCSS file of the block is imported into a file ```src/blocks/modules/_modules.scss```
    * JS file of the block is imported into ```src/js/import/modules.js```

An example of a folder structure with a BEM block:
```
blocks
├── modules
│   ├──header
│   │   ├── header.html
│   │   ├── header.js
│   │   ├── header.scss
```

In order not to manually create the corresponding folder and files, just write the following commands in the console:
* ```npm run bem-m my-block``` - to create a BEM block in ```src/block/modules``` (for the main BEM blocks), where ```my-block``` - name of block; 
* ```npm run bem-с my-component``` - to create a component in ```src/blocks/components``` (for components), where ```my-component``` - name of component

### Project Pages
* project pages are in folder ```src/views/pages```
    * home page: ```src/views/index.html```

### Fonts
* fonts are in the folder ```src/fonts```
    * use [formats](https://caniuse.com/#search=woff) ```.woff``` and ```.woff2```
    * fonts are included in the file ```src/styles/base/_fonts.scss```
    * you can convert local fonts using [this service](https://onlinefontconverter.com/)

### Images
* images are in folder ```src/img```
    * the image for generating favicons must be in the folder ```src/img/favicon``` and be at least ```1024px x 1024px```

### Third party libraries
* all third party libraries are installed in a folder ```node_modules```
    * to download them, use the command ```yarn add package_name```
    * to connect JS files of libraries, import them at the very beginning of the JS file of the BEM block (that is, the BEM block that the script uses), for example:
    ```javascript
    import $ from "jquery";
    ```
    * to connect the style files of the libraries, import them into the file ```src/styles/vendor/_libs.scss```
    * JS files and library style files cannot be changed by yourself

:WARNING: If your project uses several libraries that need to be included on several pages, in order to avoid errors, you need to:
* in path ```src/js/import``` create a folder ```pages```
* in folder ```pages``` create js file for page like, ```pageA.js```, and import there a library that will only be used on this page
    * perform the same step for additional pages
* in file ```webpack.config.js``` add js-files of pages to the entry point, example:
```javascript
entry: {
    main: "./src/js/index.js",
    pageA: "./src/js/import/pages/pageA.js",
    pageB: "./src/js/import/pages/pageB.js"
}
```
* connect the compiled js files on the required pages

