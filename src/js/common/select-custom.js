import PerfectScrollbar from 'perfect-scrollbar';

$(function () {
	$('select.custom-select').each(function () {
		new CustomSelect($(this));
	});
});

var CustomSelect = function( obj ){

	//private properties
	var _self = this,
		_obj = obj,
		_device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test( navigator.userAgent ),
		_text = $( '<span class="select__item"></span>' ),
		_wrap = $( '<div class="select"></div>' ),
		_site = $( '.site' ),
		_body = $( 'body' ),
		_opened = false,
		_popup;

	//private methods
	var _addWrap = function(){

			let arr = _obj
				.attr('class')
				.split(' ')
				.filter( item => item !== 'custom-select')
				.join(' ')

			_obj.wrap( _wrap.addClass(arr) );
			_obj.before( _text );

			_wrap = _obj.parent();

			_setCurOption();
			_selectViewType();

		},
		_hidePopup = function(){

			_opened = false;

			_popup.stop( true, false ).fadeOut( 300, function(){
				_popup.remove();
			} );

			_wrap.removeClass( 'is-open' ).removeAttr( 'data-id' );

		},
		_checkPopup = function () {

			if( _opened ){
				_hidePopup();
			} else {
				_showPopup();
			}

		},
		_constructor = function(){
			_obj[ 0 ].obj = _self;

			_addWrap();
			_onEvents();
		},
		_onEvents = function(){

			_site.on( 'click', function ( e ) {
				if ( $( e.target ).closest( _wrap ).length === 0 && _opened ){
					_hidePopup();
				}
			} );

			_obj.on( 'change', function() {
				_setCurOption();
			} );

			if( !_device ){
				_wrap.on( 'click', function(){
					_checkPopup();
				} );
			}

		},
		_setCurOption = function(){

			var curOption = _obj.find( 'option:selected' ),
				curTitle = curOption.attr( 'title' );

			if ( curTitle === undefined ){
				curTitle = curOption.text();
			}

			if ( curOption.val() == '' ) {
				_text.addClass( 'is-empty' );
			} else {
				_text.removeClass( 'is-empty' );
			}

			_text.text( curTitle );

		},
		_selectViewType = function(){

			if( _device ){
				_wrap.addClass( 'select_mobile' );
			}

			if( _obj.attr( 'required') ){
				_wrap.addClass( 'is-required' );
			}

		},
		_showPopup = function(){

			var list = $( '<ul id="select__popup-list" />'),
				offset = _wrap.offset(),
				curIndex = _obj.find( 'option:selected' ).index();

			if( _opened ){
				_popup.remove();
			}

			_opened = true;

			_popup = $( '<div id="select__popup" />' );
			_popup.append( '<div id="select__popup-wrap"><div id="select__popup-scroll"></div></div>' );

			_obj.children().each( function( i ){

				var curItem = $( this );

				curItem.attr( 'data-id', i );

				if( curItem.is( ':selected' ) ){
					list.append( '<li class="is-active" data-id="'+ i +'">' + curItem.text() + '</li>' );
				} else {
					list.append( '<li data-id="'+ i +'">' + curItem.text() + '</li>' );
				}

			} );

			var scrollContent = _popup.find( '#select__popup-scroll' );

			scrollContent.append( list );
			_body.append( _popup );

			_wrap.addClass( 'is-open' );

			_popup.css( {
				left: offset.left,
				top: offset.top + _wrap.outerHeight(),
				width: _wrap.outerWidth()
			} );

			_checkVerticalScrollContent();

			_popup.css( 'opacity', .1 );
			_popup.animate( { opacity: 1 }, 300);

			_popup.find( 'li' ).on( 'click', function(){

				var index = $( this ).attr( 'data-id' ),
					curItem = _obj.find( 'option' ).filter( '[data-id='+ index +']' );

				_obj.val( curItem.attr( 'value' ) );
				_obj.trigger( 'change' );

				_hidePopup();

			} );

		},
		_checkVerticalScrollContent = function () {

			var wrap = _popup.find( '#select__popup-wrap' ),
				scrollContent = wrap.find( '#select__popup-scroll' ),
				listWrap = wrap.find( '#select__popup-list' );

			if( listWrap.outerHeight() > scrollContent.outerHeight() ){

				wrap.addClass( 'is-scroll in-top-list' );

				var scrollWrap = document.querySelector( '#select__popup-scroll' );

				new PerfectScrollbar( scrollWrap, {
					suppressScrollX: true
				} );

			}

			scrollContent.on( 'ps-y-reach-start', function () {

				wrap.addClass( 'in-top-list' );

				scrollContent.on( 'ps-scroll-down', function () {
					wrap.removeClass( 'in-top-list' );
					scrollContent.off( 'ps-scroll-down' );
				} );

			} );
			scrollContent.on( 'ps-y-reach-end', function () {

				wrap.addClass( 'in-down-list' ).removeClass( 'in-top-list' );

				scrollContent.on( 'ps-scroll-up', function () {
					wrap.removeClass( 'in-down-list' );
					scrollContent.off( 'ps-scroll-down' );
				} );

			} );

		};

	_self.reset = function () {

		var emptyOption = _obj.find('option:not([value=0])');

		if (emptyOption.length > 0) {
			emptyOption.trigger('select');
		}
		_setCurOption();

	};

	_constructor();

};
