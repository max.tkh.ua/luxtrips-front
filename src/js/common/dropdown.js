export class Dropdown {
	constructor(_el) {
		this.el = _el;
		this.result = this.el.find('.dropdown__result');
		this.list = this.el.find('.dropdown__list');
		this.item = this.el.find('.dropdown__item');
		this.speed = 200;
		this.body = $('body');
		this.isRightPosition = null;

		this.checkParams();
		this.onEvents();
	}

	checkParams() {
		this.isRightPosition = this.el.hasClass('dropdown_right');
	}

	onEvents() {
		const that = this;

		this.body.on({
			click: () => this.hide()
		});

		this.item.on({
			click: function () {
				const el = $(this);

				if (!that.list.is(':visible')) {
					that.show();
					return
				}

				if (el.parent().hasClass('dropdown__result')) {
					that.hide();
					return
				}

				if (el.parent().hasClass('dropdown__list')) {

					that.result.find('.dropdown__item').appendTo(that.list);
					el.appendTo(that.result);

					that.hide();
				}
			}
		});

		this.el.on({
			click: () => {
				const curEvent = event || window.event;

				if (curEvent.stopPropagation) {
					curEvent.stopPropagation();
				} else {
					curEvent.cancelBubble = true;
				}
			}
		});
	}

	hide() {
		this.list.stop(true, true).slideUp(this.speed);
		this.el.removeClass('dropdown_active');

		this.setPosition();
	}

	show() {
		this.setPosition(true);

		this.list.stop(true, true).slideDown(this.speed);
		this.el.addClass('dropdown_active');
	}

	setPosition(isShow) {

		if (!this.isRightPosition) {
			this.checkRightSpace(isShow)
		}
	}

	checkRightSpace(isShow) {
		if (isShow) {
			const positionValue = this.el.position().left;
			const listWidth = this.list.innerWidth();

			if (positionValue + listWidth >= $(window).width()) {
				this.el.addClass('dropdown_right')
			}
		} else {
			setTimeout(()=>{
				this.el.removeClass('dropdown_right');
			}, this.speed)
		}
	}
}

$(function (){
	$.each($('.dropdown'), function () {
		new Dropdown($(this));
	});
})
