import $ from 'jquery'
import { Datepicker } from 'vanillajs-datepicker';
import { DateRangePicker } from 'vanillajs-datepicker';

import "./import/modules";

import "%common%/dropdown"
import "%common%/select-custom"

( function(){

	"use strict";

	$( function () {

		$.each($( '.js-datepicker-range' ), function() {
			InitDatepicker( $( this ) )
		} );

		$.each($( '.js-datepicker' ), function()  {
			new Datepicker($( this )[0], {

			});
		} );

		$.each( $( '.wpcf7' ), function() {
			new ContactForm7ContactFormChecker( $( this ) );
		} );

	} );

	var InitDatepicker = function( obj ) {

		//private properties
		var _obj = obj,
			_dateFieldFrom = _obj.find( '.js-datepicker-from' ),
			_dateFieldTo = _obj.find( '.js-datepicker-to' ),
			_dateFrom, _dateTo;

		//private methods
		var _onEvent = function() {

				_dateFieldFrom.on( 'pick.datepicker', function ( e ) {
					_dateTo = _dateFieldTo.datepicker(
						'setStartDate', new Date( e.date )
					);
				});

				_dateFieldTo.on( 'pick.datepicker', function ( e ) {
					_dateFrom = _dateFieldFrom.datepicker(
						'setEndDate', new Date( e.date )
					);
				});

			},
			_initDatepickers = function() {

				_dateFrom = _dateFieldFrom.datepicker( {
					language: 'ru-RU',
					autoHide: true,
					startDate: new Date()
				} );

				_dateTo = _dateFieldTo.datepicker( {
					language: 'ru-RU',
					autoHide: true,
					startDate: new Date()
				} );

			},
			_construct = function() {
				_onEvent();
				_initDatepickers();
			};

		_construct();
	};

	class ContactForm7ContactFormChecker {

		constructor(_el) {
			this._wrap = _el;
			this._form = this._wrap.find('form');
			this._submitBtn = this._form.find('button');

			this.onEvent();
		}

		showPreload() {
			this._submitBtn.prop('disabled', true);
			this._submitBtn.addClass('is-loading');

			this._submitBtn.append('<div class="loader"><ul><li><hr/><hr/><hr/><hr/></li><li><hr/><hr/><hr/><hr/></li><li><hr/><hr/><hr/><hr/></li></ul></div>');
		}

		hidePreload() {
			this._submitBtn.prop('disabled', false);
			this._submitBtn.removeClass('is-loading');

			this._submitBtn.find('.loader').remove();
		}

		showSuccessMessage () {
			this.hidePreload();
			$('.popup').trigger('openThanksPopup');
		}

		onEvent() {
			const that = this;

			this._form.on( 'submit', function () {
				that.showPreload();
			} );

			this._wrap.on( {
				'wpcf7invalid': function(){
					that.hidePreload();
				},
				'wpcf7spam': function(){
					that.hidePreload();
				},
				'wpcf7mailsent': function(){
					that.showSuccessMessage();
				},
				'wpcf7mailfailed': function(){
					that.hidePreload();
				}
			} );

		}

	}

} )();
