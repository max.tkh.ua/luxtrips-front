import "%components%/filter/filter"
import "%components%/map/map"
import "%components%/popup/popup"

import "%modules%/hero/hero"
import "%modules%/top-experiences/top-experiences"
import "%modules%/luxury-packages/luxury-packages"
import "%modules%/package-info/package-info"
import "%modules%/reviews/reviews"
