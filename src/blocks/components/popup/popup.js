class Popup {

	constructor(_el) {
		this.btnShow = $( '[data-popup]' );
		this.obj = _el;
		this.popupWrap = this.obj.find( '.popup__wrap' );
		this.popupContents = this.obj.find( '.popup__content' );
		this.btnClose = this.obj.find( '.popup__close, .popup__cancel' );
		this.siteHeader = $( '.site__header-layout' );
		this.videoFile;
		this.curBtn;
		this.scrollConteiner = $( 'html' );
		this.body = $( 'body' );
		this.site = this.body.find( '.site' );
		this.url = this.obj.data( 'action' );
		this.position = 0;
		this.window = $( window );

		this.onEvents();
	}

	getScrollWidth (){
		var scrollDiv = document.createElement( 'div'),
			scrollBarWidth;

		scrollDiv.className = 'popup__scrollbar-measure';

		document.body.appendChild( scrollDiv );

		scrollBarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

		document.body.removeChild(scrollDiv);

		return scrollBarWidth;
	}

	hidePopup (){
		const that = this;

		this.obj.addClass( 'is-hide' ).removeClass( 'is-opened' );

		this.obj.on( 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {

			that.scrollConteiner.removeAttr( 'style' );
			that.body.removeAttr( 'style' );
			that.site.removeAttr( 'style' );
			that.siteHeader.removeAttr( 'style' );
			that.obj.removeAttr( 'style' );

			that.window.scrollTop( that.position );
			that.position = 0;

			that.obj.removeClass( 'is-hide' );

			if ( that.videoFile != null ) {
				that.videoFile.remove();
				that.videoFile = null;
			}

			that.obj.addClass( 'is-loading' );

			that.obj.off( 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend' );

		} );

	}

	onEvents (){
		const that = this;

		this.obj.on( 'click', function (e) {
			if ( $( e.target ).closest( that.popupContents ).length === 0 ){
				that.hidePopup();
			}
		} );

		this.btnShow.on( 'click', function(e) {
			e.preventDefault();
			that.curBtn = $(this);
			that.showPopup($(this).data( 'popup' ));
		} );

		this.btnClose.on( 'click', function(e){
			e.preventDefault();
			that.hidePopup();
		} );

		this.obj.on( 'openThanksPopup', () => {
			that.showPopup('thanks');
			this.curBtn = null;
		} )
	}

	showPopup (name){

		this.setPopupContent(name);

		if ( this.window.scrollTop() !== 0 ){
			this.position = this.window.scrollTop();
		}

		this.scrollConteiner.css( {
			overflowY: 'hidden',
			paddingRight: this.getScrollWidth()
		} );

		this.body.css( 'overflow-y', 'hidden' );

		this.site.css( {
			'position': 'relative',
			'top': this.position * -1
		} );

		if ( this.popupWrap.outerHeight() <= this.window.outerHeight() ) {
			this.obj.css( {
				paddingRight: this.getScrollWidth()
			} );
		}

		this.obj.addClass( 'is-opened' );
	}

	setPopupContent (className){

		var curContent = this.popupContents.filter( '.popup-' + className );

		this.popupContents.css( { display: 'none' } );
		curContent.css( { display: 'flex' } );

		if ( className === 'video' ) { this.setVideoFile( this.curBtn.data( 'url' ), curContent ) }
		else if ( this.curBtn && this.curBtn.attr('data-project') ) { this.setProjectInput(); }
		else if ( className === 'calculate' ) { this.calculate.updateState() }
		else { this.obj.removeClass( 'is-loading' ); }

	}

	setProjectInput () {

		const projectField = this.obj.find( '#your-project' );

		projectField.val( this.curBtn.data( 'project' ) );

	}

	setVideoFile  (id, obj) {
		const that = this;

		var curContent = obj;
		var videoFrame = $('<iframe src="https://www.youtube.com/embed/'+ id +'?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');

		curContent.append( videoFrame );

		this.videoFile = curContent.find('iframe');

		this.videoFile.on( 'load', function () {
			that.setVideoSize();
			that.obj.removeClass( 'is-loading' );
		} );

		this.window.on( 'resize', function () {
			that.setVideoSize();
		} );

	}

	setVideoSize  () {
		var videoWidth = this.videoFile.outerWidth();
		this.videoFile.css( 'height', videoWidth / 1.7777 + 'px' );
	}
}

$.each( $( '.popup' ), function() {
	new Popup( $( this ) );
} );
