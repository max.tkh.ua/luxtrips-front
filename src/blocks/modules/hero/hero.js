function ElementsSizeOnDesktop(){

	//private properties
	const _hero = $('.hero')
	const _window = $(window)

	//private methods
	var _construct = function(){
			_onEvent();
			_proportionalElements();
		},
		_onEvent = function () {

			_window.on( 'resize', function () {
				_proportionalElements();
			} );

		},
		_proportionalElements = function () {
			if ( _window.outerWidth() >= 768 ) {

				var koef = _window.outerWidth() * 100 / 1440;

				_hero.css( 'font-size',  koef + 'px' );

			} else {
				_hero.removeAttr( 'style' );
			}
		};

	//public properties

	//public methods

	_construct();

}

ElementsSizeOnDesktop()
