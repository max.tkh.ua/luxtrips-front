import Swiper, {Navigation, Pagination} from 'swiper'

class ReviewsSlider {

	constructor(_el) {
		this.topExperiences = _el
		this.slider = this.topExperiences.find('.reviews__list')
		this.buttonLeft = this.topExperiences.find('.slider__arrow-left')
		this.buttonRight = this.topExperiences.find('.slider__arrow-right')
		this.pagination = this.topExperiences.find('.reviews__pagination')
		this.swiper = null

		this.initSwiper();
	}

	initSwiper() {

		this.swiper = new Swiper(this.slider[0], {
			modules: [Navigation, Pagination],
			slidesPerView: 'auto',
			spaceBetween: 16,
			slideActiveClass: 'active',
			breakpoints: {
				768: {
					spaceBetween: 94
				},
			},
			navigation: {
				nextEl: this.buttonRight[0],
				prevEl: this.buttonLeft[0],
			},
			pagination: {
				el: this.pagination[0],
				clickable: true
			}
		});
	}
}

$(function (){
	$.each($('.reviews'), function () {
		new ReviewsSlider($(this));
	});
})
