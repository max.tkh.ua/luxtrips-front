import Swiper, {Navigation, Pagination} from 'swiper'

class LuxuryPackagesSlider {

	constructor(_el) {
		this.topExperiences = _el
		this.slider = this.topExperiences.find('.luxury-packages__list')
		this.buttonLeft = this.topExperiences.find('.slider__arrow-left')
		this.buttonRight = this.topExperiences.find('.slider__arrow-right')
		this.pagination = this.topExperiences.find('.luxury-packages__pagination')
		this.swiper = null

		this.initSwiper();
		this.onEvent();
	}

	onEvent() {

		window.addEventListener('resize', () => {
			if(window.matchMedia('(min-width: 1024px)').matches && this.swiper) {
				this.swiper.destroy()
				this.swiper = null
			} else if(!window.matchMedia('(min-width: 1024px)').matches && !this.swiper) {
				this.initSwiper();
			}
		})

	}

	initSwiper() {

		if(window.matchMedia('(min-width: 1024px)').matches) return

		this.swiper = new Swiper(this.slider[0], {
			modules: [Navigation, Pagination],
			spaceBetween: 16,
			width: 320,
			slideActiveClass: 'active',
			breakpoints: {
				768: {
					spaceBetween: 30
				},
			},
			navigation: {
				nextEl: this.buttonRight[0],
				prevEl: this.buttonLeft[0],
			},
			pagination: {
				el: this.pagination[0],
				clickable: true
			}
		});
	}
}

$(function (){
	$.each($('.luxury-packages'), function () {
		new LuxuryPackagesSlider($(this));
	});
})
