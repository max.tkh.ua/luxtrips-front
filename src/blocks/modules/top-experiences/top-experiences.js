import $ from 'jquery'
import Swiper, {Thumbs, Navigation, Pagination} from 'swiper'

class ExpertiseSlider {

	constructor(_el) {
		this.topExperiences = _el
		this.slider = this.topExperiences.find('.top-experiences__list')
		this.categories = this.topExperiences.find('.top-experiences__categories')
		this.category = this.topExperiences.find('.top-experiences__category')
		this.categoriesWrapWidth = this.sumItemsWidth()
		this.buttonLeft = this.topExperiences.find('.slider__arrow-left')
		this.buttonRight = this.topExperiences.find('.slider__arrow-right')
		this.pagination = this.topExperiences.find('.top-experiences__pagination')

		this.initSwiper();
		this.onEvent();
	}

	onEvent() {

	}

	sumItemsWidth () {
		let curItem = 0;

		this.category.each(function () {
			curItem += $(this).outerWidth(true);
		} )

		return curItem;
	}

	initSwiper() {

		new Swiper(this.categories[0], {
			slidesPerView: 'auto',
			centeredSlides: true,
			spaceBetween: 40,
			threshold: 10,
			freeMode: true,
			breakpoints: {
				768: {
					spaceBetween: 37,
					centeredSlides: false,
				},
			},
		});

		new Swiper(this.slider[0], {
			modules: [Thumbs, Navigation, Pagination],
			spaceBetween: 16,
			slidesPerView: 'auto',
			slideActiveClass: 'active',
			breakpoints: {
				768: {
					spaceBetween: 30
				},
			},
			pagination: {
				el: this.pagination[0],
				clickable: true
			},
			navigation: {
				nextEl: this.buttonRight[0],
				prevEl: this.buttonLeft[0],
			},
		});
	}
}

new ExpertiseSlider( $('.top-experiences') )
