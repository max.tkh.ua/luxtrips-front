import Swiper, {Thumbs, EffectFade} from 'swiper'

class PackageInfoTabs {

	constructor(_el) {
		this.wrap = _el
		this.slider = this.wrap.find('.package-info__list')
		this.pagination = this.wrap.find('.package-info__categories')

		this.initSwiper();
	}

	initSwiper() {

		const pagination = new Swiper(this.pagination[0], {
			slidesPerView: 'auto',
			centeredSlides: true,
			spaceBetween: 32,
			threshold: 10,
			freeMode: true,
			breakpoints: {
				768: {
					spaceBetween: 52,
					centeredSlides: false
				},
				1024: {
					spaceBetween: 0,
					centeredSlides: false
				},
			},
		});

		new Swiper(this.slider[0], {
			modules: [EffectFade, Thumbs],
			effect: 'fade',
			allowTouchMove: false,
			autoHeight: true,
			spaceBetween: 16,
			slideActiveClass: 'is-active',
			thumbs: {
				swiper: pagination,
				slideThumbActiveClass: 'is-active',
				multipleActiveThumbs: false
			},
			on: {
				activeIndexChange() {
					pagination.slideTo(this.activeIndex)
				}
			}
		});
	}
}

$(function (){
	$.each($('.package-info'), function () {
		new PackageInfoTabs($(this));
	});
})
